package org.arijit;

import javax.security.auth.login.AppConfigurationEntry;

import org.arijit.customers.Customer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.remoting.soap.SoapFaultException;

public class Test {
	public static void main(String[] args) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
		Customer customer1 = context.getBean("ccustomer",Customer.class);
		Customer customer2 = context.getBean("scustomer",Customer.class);
		
		System.out.println(customer1.accountInfo());
		System.out.println(customer2.accountInfo());

	}
}
