package org.arijit.customers;

import org.arijit.interfaces.Account;
import org.springframework.beans.factory.annotation.Autowired;

public class Customer {
	private String name;
	private String userName;
	private String password;
	private int age;
	private String ssn;
	private String address;
	private String email;
	
	
	
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Customer(String name, String userName, String password, int age, String ssn, String address, String email,
			long phone, float balance) {
		super();
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.age = age;
		this.ssn = ssn;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.balance = balance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getPhone() {
		return phone;
	}
	public void setPhone(long phone) {
		this.phone = phone;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	private long phone;
	private  float balance;



	public String accountInfo() {
		// TODO Auto-generated method stub
		return null;
	}
}
//i. Name-String
//ii. Username-String
//iii. Password-String
//iv. Age-int
//v. SSN-String
//vi. Address-String(just city name)
//vii. Email-String
//viii. Phone-long
//ix. Balance-float