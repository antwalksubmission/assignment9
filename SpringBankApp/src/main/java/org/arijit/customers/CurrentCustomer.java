package org.arijit.customers;

import org.arijit.interfaces.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class CurrentCustomer extends Customer{
	
	@Autowired
	@Qualifier("current")
	Account account;
	
	public String accountInfo() {
		return account.showAccount();
	}

	public CurrentCustomer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CurrentCustomer(String name, String userName, String password, int age, String ssn, String address,
			String email, long phone, float balance) {
		super(name, userName, password, age, ssn, address, email, phone, balance);
		// TODO Auto-generated constructor stub
	}
}
