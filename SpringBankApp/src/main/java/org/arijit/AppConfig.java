package org.arijit;

import org.arijit.accounts.Current;
import org.arijit.accounts.Saving;
import org.arijit.customers.CurrentCustomer;
import org.arijit.customers.SavingCustomer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("org.arijit")
public class AppConfig {
	
	@Bean("current")
	public Current getCurrent()
	{
		return new Current();
	}
	
	@Bean("saving")
	public Saving getSavings() {
		return new Saving();
	}
	
	@Bean("ccustomer")
	public  CurrentCustomer getCurrentCustomer() {
		return new CurrentCustomer();
	}
	
	@Bean("scustomer")
	public SavingCustomer getSavingCustomer() {
		return new SavingCustomer();
	}

}
